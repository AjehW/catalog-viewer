import data from './data.json';
import './App.css'

function App() {

  return (
    <div className="App" style={{display: 'block'}}>
      <div className="nav">
        <img className="logo" src={data.imageUri} alt="Logo" />
        <h1 id="header">~ {data.name} ~</h1>
      </div>
      <div className="productHolder">
      {data?.products.map((product, idx)=>
        <div className="productCard" key={idx}>
            <h2>{product.name}</h2>
            <span> --- </span>
            <h3>{product.author ? (<p>{product.author}</p>) : (<p>No Author Name Available</p>)}</h3>
            <p className="price">${product.retailPrice}</p>
            <div className="imgHolder" style={ product.images ? { backgroundImage: `url('${product.images[0].uri}')`} : {backgroundColor: 'FFFFFF'}}></div>
        </div>
          )
     }
      </div>
    </div>
  );
}

export default App;
